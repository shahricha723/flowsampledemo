package com.example.usecasedemo.utils

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
