package com.example.usecasedemo.repository

import com.example.usecasedemo.api.ApiService
import com.example.usecasedemo.model.DataJackSon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class UserRepository @Inject constructor(private val apiService: ApiService) {


    suspend fun getUsersWithFlow(): Flow<DataJackSon> = flow {
        emit(apiService.getUser())
    }.flowOn(Dispatchers.IO)

}