package com.example.usecasedemo.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.usecasedemo.R
import com.example.usecasedemo.adapters.UserListAdapter
import com.example.usecasedemo.database.UserDataDAO
import com.example.usecasedemo.databinding.FragmentUserListBinding
import com.example.usecasedemo.model.DataJackSon
import com.example.usecasedemo.model.UserData
import com.example.usecasedemo.utils.ApiState
import com.example.usecasedemo.utils.Constant.ID
import com.example.usecasedemo.utils.hideView
import com.example.usecasedemo.utils.showView
import com.example.usecasedemo.viewmodel.UserViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class UserListFragment : Fragment() {
    private lateinit var binding: FragmentUserListBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var userListAdapter: UserListAdapter

    @Inject
    lateinit var userDataDao: UserDataDAO

    @Inject
    lateinit var mGoogleSignInClient: GoogleSignInClient

    private var userId: Int? = null
    private var firstName: String? = null
    private var lastName: String? = null
    private var email: String? = null
    private var avatar: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_list, container, false)

        observeUserDataWithFlow()
        userViewModel.getUsersWithFlow()


        binding.ibLogout.setOnClickListener {
            Toast.makeText(requireContext(), "clicked", Toast.LENGTH_SHORT).show()
            userViewModel.logOutUser()
            mGoogleSignInClient.signOut()
            CoroutineScope(Dispatchers.IO).launch {
                userDataDao.clearDataBase()
            }
            findNavController().navigate(R.id.action_userListFragment_to_loginFragment)
        }

        return binding.root
    }


    private fun observeUserDataWithFlow() {
        lifecycleScope.launchWhenStarted {
            userViewModel.userStateFlow.collect {
                when (it) {
                    is ApiState.Success -> {
                        binding.pb.hideView()
                        binding.rvUsers.showView()
                        it.data.data.let { userList ->
                            setUpUserAdapter(userList = userList)
                            //insertUserDataToDB(userList)
                            //manageData()
                        }
                    }
                    is ApiState.Failure -> {
                        binding.pb.hideView()
                        binding.rvUsers.showView()
                        Toast.makeText(requireContext(), "Error : ${it.msg}", Toast.LENGTH_SHORT)
                            .show()

                    }
                    is ApiState.Loading -> {
                        binding.pb.showView()
                        binding.rvUsers.hideView()
                    }
                    is ApiState.Empty -> {

                    }
                }

            }
        }
    }

    private fun insertUserDataToDB(dataBean: List<DataJackSon.Data?>?) {
        dataBean?.forEach {
            it?.let { user ->
                userId = user.id
                firstName = user.firstName
                lastName = user.lastName
                email = user.email
                avatar = user.avatar
            }
            CoroutineScope(Dispatchers.IO).launch {
                userDataDao.addUser(
                    UserData(
                        userID = userId,
                        avatar = avatar,
                        email = email,
                        firstName = firstName,
                        lastName = lastName
                    )
                )
            }
        }

    }


    private fun setUpUserAdapter(userList: List<DataJackSon.Data?>?) {
        userListAdapter = UserListAdapter(userList) {
            val bundle = Bundle()
            bundle.putInt(ID, it)
            findNavController().navigate(
                R.id.action_userListFragment_to_userDetailsFragment, bundle)
        }
        binding.rvUsers.adapter = userListAdapter
        userViewModel.getUserList()
    }

}