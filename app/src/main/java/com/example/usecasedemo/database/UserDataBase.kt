package com.example.usecasedemo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.usecasedemo.model.UserData

@Database(entities = [UserData::class], version = 2)
abstract class UserDataBase : RoomDatabase() {

    abstract fun getDao(): UserDataDAO

}