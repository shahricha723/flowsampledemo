package com.example.usecasedemo.model


import com.fasterxml.jackson.annotation.JsonProperty

data class DataJackSon(
    @JsonProperty("data") var `data`: List<Data?>? = null,
    @JsonProperty("page") var page: Int? = null,
    @JsonProperty("per_page") var perPage: Int? = null,
    @JsonProperty("support") var support: Support? = null,
    @JsonProperty("total") var total: Int? = null,
    @JsonProperty("total_pages") var totalPages: Int? = null
) {
    data class Data(
        @JsonProperty("avatar") var avatar: String? = null,
        @JsonProperty("email") var email: String? = null,
        @JsonProperty("first_name") var firstName: String? = null,
        @JsonProperty("id") var id: Int? = null,
        @JsonProperty("last_name") var lastName: String? = null
    )

    data class Support(
        @JsonProperty("text") var text: String? = null, @JsonProperty("url") var url: String? = null
    )
}