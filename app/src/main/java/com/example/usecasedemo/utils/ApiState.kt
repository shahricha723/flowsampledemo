package com.example.usecasedemo.utils

import com.example.usecasedemo.model.DataJackSon

sealed class ApiState{
    object Loading : ApiState()
    class Failure(val msg:Throwable) : ApiState()
    class Success(val data: DataJackSon) : ApiState()
    object Empty : ApiState()
}