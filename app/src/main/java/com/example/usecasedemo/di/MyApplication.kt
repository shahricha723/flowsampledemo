package com.example.usecasedemo.di

import android.app.Application
import com.example.usecasedemo.datastorepref.DataStoreImpl
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication : Application() {

}