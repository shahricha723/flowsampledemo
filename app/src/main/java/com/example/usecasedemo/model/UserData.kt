package com.example.usecasedemo.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonProperty
import org.intellij.lang.annotations.PrintFormat

@Entity
data class UserData(
    @PrimaryKey(autoGenerate = true) var id: Int? = null,
    var avatar: String? = null,
    var email: String? = null,
    var userID: Int? = null,
    var firstName: String? = null,
    var lastName: String? = null
)
