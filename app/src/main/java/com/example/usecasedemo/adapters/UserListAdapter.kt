package com.example.usecasedemo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.usecasedemo.R
import com.example.usecasedemo.databinding.ItemUserBinding
import com.example.usecasedemo.model.DataJackSon


class UserListAdapter(
    private val userList: List<DataJackSon.Data?>?,
    private val clickCallBack: ((Int) -> Unit)
) :
    RecyclerView.Adapter<UserListAdapter.ViewHolder>() {


    inner class ViewHolder(val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemUserBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_user,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = userList!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        userList?.get(position).let {
            if (it != null) {
                onBind(holder.binding, it)
            }
        }
        holder.itemView.setOnClickListener {
            userList?.get(position)?.id?.let { it1 -> clickCallBack.invoke(it1) }
        }
    }

    private fun onBind(binding: ItemUserBinding, data: DataJackSon.Data) {
        binding.myData = data
    }


}