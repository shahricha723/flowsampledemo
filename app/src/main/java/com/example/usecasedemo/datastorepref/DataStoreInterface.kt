package com.example.usecasedemo.datastorepref

import kotlinx.coroutines.flow.Flow

interface DataStoreInterface {
    suspend fun saveString(key: String, value: String)

    suspend fun getString(key: String): Flow<String?>

    suspend fun clearPrefValue(key: String)

    suspend fun clearAllPref()

    suspend fun saveBoolean(key: String, isLogin: Boolean)


    val email: Flow<String>

}