package com.example.usecasedemo.view.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.usecasedemo.R
import com.example.usecasedemo.databinding.FragmentUserDetailsBinding
import com.example.usecasedemo.utils.ApiState
import com.example.usecasedemo.utils.Constant.ID
import com.example.usecasedemo.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.*
@AndroidEntryPoint
class UserDetailsFragment : Fragment() {

    private lateinit var binding: FragmentUserDetailsBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private var id: Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {

        getData()
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_details, container, false)

        observeUserDetails()

        binding.btnSave.setOnClickListener {
//            findNavController().navigate(R.id.action_userListFragment_to_userDetailsFragment)
					findNavController().popBackStack()
        }

        return binding.root
    }

    private fun getData() {
        arguments?.let {
            id = it.getInt(ID)
        }
    }

    private fun observeUserDetails() {
        lifecycleScope.launchWhenStarted {
            userViewModel.userStateFlow.collect {
                when (it) {
                    is ApiState.Success -> {
                        val mData = it.data.data
                        val user = mData?.first { users -> users?.id == id }
                        binding.myData = user
                    }
                    is ApiState.Failure -> {

                    }
                    is ApiState.Loading -> {

                    }
                    is ApiState.Empty -> {

                    }
                }
            }
        }
    }

}