package com.example.usecasedemo.api

import com.example.usecasedemo.model.DataJackSon
import retrofit2.http.GET

interface ApiService {
    @GET("/api/users")
    suspend fun getUser(): DataJackSon
}