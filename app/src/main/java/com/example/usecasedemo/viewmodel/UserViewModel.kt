package com.example.usecasedemo.viewmodel

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.usecasedemo.database.UserDataDAO
import com.example.usecasedemo.datastorepref.DataStoreImpl
import com.example.usecasedemo.model.UserData
import com.example.usecasedemo.repository.UserRepository
import com.example.usecasedemo.utils.ApiState
import com.example.usecasedemo.utils.Constant.IS_LOGIN
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class UserViewModel @Inject constructor(
    private val mainRepository: UserRepository,
    private val dataStore: DataStoreImpl,
    private val userDataDao: UserDataDAO
) : ViewModel() {

    private val _userStateFlow: MutableStateFlow<ApiState> = MutableStateFlow(ApiState.Empty)
    val userStateFlow: StateFlow<ApiState> = _userStateFlow

    var userFromDBFlow: Flow<List<UserData>> = flowOf()

    fun getUsersWithFlow() = viewModelScope.launch {
        _userStateFlow.value = ApiState.Loading
        mainRepository.getUsersWithFlow().catch { error ->
            _userStateFlow.value = ApiState.Failure(error)
        }.collect { data ->
            _userStateFlow.value = ApiState.Success(data)
        }
    }


    fun saveBooleanDataTOPref(key: String, value: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
	        dataStore.saveBoolean(key, value)
        }
    }

    fun logOutUser() {
        viewModelScope.launch(Dispatchers.IO) {
	        dataStore.clearPrefValue(IS_LOGIN)
        }
    }

    fun getUserList() {
        CoroutineScope(Dispatchers.IO).launch {
            userFromDBFlow = flow {
                emit(userDataDao.getUser())
            }
        }
    }


}