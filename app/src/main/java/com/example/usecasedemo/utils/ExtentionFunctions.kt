package com.example.usecasedemo.utils

import android.view.View

fun View.showView() {
    visibility = View.VISIBLE
}

fun View.hideView() {
    visibility = View.GONE
}
