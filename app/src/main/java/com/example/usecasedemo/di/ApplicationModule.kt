package com.example.usecasedemo.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.example.usecasedemo.R
import com.example.usecasedemo.api.ApiService
import com.example.usecasedemo.database.UserDataBase
import com.example.usecasedemo.database.UserDataDAO
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.GsonBuilder
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {


    companion object {
        const val BASE_URL = "https://reqres.in/"
    }

    @Provides
    @Singleton
    fun provideRetrofit(
    ): Retrofit {
        val builder = OkHttpClient.Builder().addInterceptor(OkHttpProfilerInterceptor())
        val okHttpProfilerClient = builder.build()
        return Retrofit.Builder()
            .addConverterFactory(JacksonConverterFactory.create())
            .baseUrl(BASE_URL).client(okHttpProfilerClient).build()
    }

    @Provides
    @Singleton
    fun provideRetrofitService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)


    @Provides
    @Singleton
    fun provideDataStore(@ApplicationContext context: Context): DataStore<Preferences> =
        preferencesDataStore(
            name = "USECASE_DATASTORE"
        ).getValue(context, String::javaClass)

    @Provides
    @Singleton
    fun provideGoogleSignInClient(@ApplicationContext context: Context): GoogleSignInClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.web_client_id)).requestEmail().build()
        return GoogleSignIn.getClient(context, gso)
    }

    @Provides
    @Singleton
    fun provideRoomDataBase(@ApplicationContext context: Context): UserDataBase =
        Room.databaseBuilder(context, UserDataBase::class.java, "user_database").build()

    @Provides
    @Singleton
    fun provideDao(userRoomDatabase: UserDataBase): UserDataDAO =
        userRoomDatabase.getDao()

}