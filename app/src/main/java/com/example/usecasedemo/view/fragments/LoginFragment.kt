package com.example.usecasedemo.view.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.usecasedemo.R
import com.example.usecasedemo.databinding.FragmentLoginBinding
import com.example.usecasedemo.datastorepref.DataStoreImpl
import com.example.usecasedemo.utils.Constant.EMAIL
import com.example.usecasedemo.utils.Constant.IS_LOGIN
import com.example.usecasedemo.utils.showView
import com.example.usecasedemo.viewmodel.UserViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var mGoogleSignInClient: GoogleSignInClient

    @Inject
    lateinit var dataStore: DataStoreImpl

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.btnSignInWithGoogle.showView()
        firebaseAuth = FirebaseAuth.getInstance()


        val signInActivityLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK) {
                    val userData = GoogleSignIn.getSignedInAccountFromIntent(it.data).result
                    userData.email?.let { email ->
                        userViewModel.saveBooleanDataTOPref(IS_LOGIN, true)
                    }
                    findNavController().navigate(R.id.action_loginFragment_to_userListFragment,args = null,
                        navOptions {
                            anim {
                                this.exit
                            }
                        })
                }
            }

        binding.btnSignInWithGoogle.setOnClickListener {
		        userViewModel.saveBooleanDataTOPref(IS_LOGIN, true)
	        findNavController().navigate(R.id.action_loginFragment_to_userListFragment,args = null,
		        navOptions {
			        anim {
				        this.exit
			        }
		        })
       /*     val signInIntent: Intent = mGoogleSignInClient.signInIntent
            signInActivityLauncher.launch(signInIntent)*/
        }

        return binding.root

    }


}