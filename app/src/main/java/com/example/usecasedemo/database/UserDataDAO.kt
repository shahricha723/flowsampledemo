package com.example.usecasedemo.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.usecasedemo.model.UserData


@Dao
interface UserDataDAO {

    @Insert
    suspend fun addUser(userData: UserData)

    @Insert
    suspend fun addUserList(userData: List<UserData>)

    @Query("SELECT * FROM userdata")
    suspend fun getUser(): List<UserData>

    @Query("DELETE FROM userdata")
    suspend fun clearDataBase()

}