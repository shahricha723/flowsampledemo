package com.example.usecasedemo.datastorepref

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.usecasedemo.utils.Constant.EMAIL
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataStoreImpl @Inject constructor(
    val dataStore: DataStore<Preferences>,
) : DataStoreInterface {


    override suspend fun saveString(key: String, value: String) {
        dataStore.edit { it ->
            it[stringPreferencesKey(key)] = value
        }
    }

    override suspend fun getString(key: String): Flow<String?> = dataStore.data.map { it ->
        it[stringPreferencesKey(key)].toString()
    }

    override suspend fun clearPrefValue(key: String) {
        dataStore.edit { it ->
            it.remove(stringPreferencesKey(key))
        }
    }

    override suspend fun clearAllPref() {
        dataStore.edit { it ->
            it.clear()
        }
    }

    override suspend fun saveBoolean(key: String, isLogin: Boolean) {
        dataStore.edit { it ->
            it[booleanPreferencesKey(key)] = isLogin
        }
    }



    override val email: Flow<String>
        get() = dataStore.data.map {
            val email = it[stringPreferencesKey(EMAIL)]
            email.toString()
        }

}