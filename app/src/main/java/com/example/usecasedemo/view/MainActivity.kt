package com.example.usecasedemo.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.usecasedemo.R
import com.example.usecasedemo.databinding.ActivityMainBinding
import com.example.usecasedemo.utils.Constant
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var dataStore: DataStore<Preferences>

    private val isLogin: Flow<Boolean?>
        get() = dataStore.data.map {
            val isLogin = it[booleanPreferencesKey(Constant.IS_LOGIN)]
            isLogin ?: false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        val navHost =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val navController = navHost!!.navController

        val navInflater = navController.navInflater
        val graph = navInflater.inflate(R.navigation.nav_graph_main)

        lifecycleScope.launch(Dispatchers.Main) {
            isLogin.collect {
                if (it == true) {
                    graph.setStartDestination(R.id.userListFragment)
                    binding.navHostFragment.findNavController().graph = graph
                } else {
                    graph.setStartDestination(R.id.loginFragment)
                    binding.navHostFragment.findNavController().graph = graph
                }
            }
        }
    }

	override fun onNavigateUp(): Boolean {
		return super.onNavigateUp()
	}
/*	override fun onSupportNavigateUp(): Boolean {
		return NavigationUI.navigateUp(navController = )
			 || super
			.onSupportNavigateUp()
	}*/
	override fun onDestroy() {
        super.onDestroy()
        lifecycleScope.launch {
            dataStore.edit {
                it.clear()
            }
        }

    }
}